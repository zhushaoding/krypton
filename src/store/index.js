import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        // 保存token信息
        tokenInfo: localStorage.getItem('jwt'),  // 存储token字符串
        userInfo: localStorage.getItem('user'),   // 存储用户信息
        // 保存购物车信息
        cartInfo: localStorage.getItem('cartInfo'),   // 存储购物车信息
        // 保存秒杀商品信息
        seckillSkuInfo: {},
        // 收货地址
        address: {
            id: '2',
            name: '李四',
            tel: '1310000000',
            address: '浙江省杭州市拱墅区莫干山路 50 号',
        },
    },
    getters: {
        getCartInfo(state) {
            return state.cartInfo
        },
        getSeckillSkuInfo(state) {
            return state.seckillSkuInfo
        },
        getAddress(state) {
            return state.address
        },
        getUserInfo(state) {
            return JSON.parse(state.userInfo);
        }
    },
    mutations: {
        /**
         * 保存token信息到state与LocalStorage
         * @param {*} state
         * @param {*} tokenInfo
         */
        saveToken(state, tokenInfo) {
            state.tokenInfo = tokenInfo
            localStorage.setItem('jwt', tokenInfo)
        },
        /**
         * 存储用户信息
         * @param {*} state
         * @param {*} userInfo
         */
        updateUserInfo(state, userInfo){
            state.userInfo = userInfo;
            localStorage.setItem('user',JSON.stringify(userInfo))
            localStorage.setItem('username',userInfo.username);
            localStorage.setItem('userId',userInfo.userId);
        },
    },
    actions: {},
    modules: {}
})
