import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import Vant from 'vant';
import 'vant/lib/index.css';
import '@vant/touch-emulator';
import request from "@/utils/request";
import { Lazyload } from 'vant';
import qs from  'qs'

Vue.prototype.axios = request;
Vue.prototype.qs = qs
Vue.use(Vant);
Vue.use(Lazyload, {
  lazyComponent: true,
});
Vue.config.productionTip = false
Vue.prototype.map


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
