import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/HomeView.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/home',
  },
  {
    path: '/home',
    component: HomeView
  },

  {
    path: '/product/:id/detail',
    component: () => import( '../views/product/Detail.vue')
  },
  {
    path: '/product/:id/seckill/detail',
    component: () => import( '../views/product/SeckillDetail.vue')
  },
  {
    path: '/product/search',
    component: () => import( '../views/product/SearchList.vue')
  },
  {
    path: '/product/search/:keyword',
    component: () => import( '../views/product/SearchList.vue')
  },
  {
    path: '/category',
    component: () => import( '../views/product/categoryBeta')
  },
  {
    path: '/cart',
    component: () => import( '../views/user/shoppingCart.vue')
  },
  {
    path: '/messages',
    component: () => import( '../views/user/websocket')
  },
  {
    path: '/xxx',
    component: () => import( '../views/user/HelloWorld')
  },
  {
    path: '/user',
    component: () => import( '../views/user/User.vue')
  },
  {
    path: '/login',
    component: () => import( '../views/user/login.vue')
  },
  {
    path: '/message',
    component: () => import( '../views/user/Message.vue')
  },
  {
    path: '/order/list',
    component: () => import( '../views/order/List.vue')
  },
  {
    path: '/order/detail',
    component: () => import( '../views/order/Detail.vue')
  },
  {
    path: '/order/generate',
    component: () => import('../views/order/Generate.vue')
  },
  ,
  {
    path: '/order/payorder-method',
    component: () => import('../views/order/PayOrderMethod.vue')
  },
  ,
  {
    path: '/order/payorder-success',
    component: () => import('../views/order/PayOrderSuccess.vue')
  },
  {
    path: '/order/addresslist',
    component: () => import('../views/order/AddressList.vue')
  },
  {
    path: '/user',
    component: () => import( '../views/user/User.vue')
  },
  {
    path: '/user/center',
    component: () => import( '../views/user/PersonalCenter.vue')
  },
  {
    path: '/user/part/address',
    component: () => import( '../views/user/part/Address.vue')
  },
  {
    path: '/user/part/businesslicense',
    component: () => import( '../views/user/part/businesslicense.vue')
  },
  {
    path: '/user/part/rotocolRules',
    component: () => import( '../views/user/part/rotocolRules.vue')
  },
  {
    path: '/user/part/management',
    component: () => import( '../views/user/part/privacymanagement.vue')
  },

  {
    path: '/test',
    component: () => import( '../views/test/test')
  },


]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
